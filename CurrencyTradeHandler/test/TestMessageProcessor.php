<?php

require_once '../server/Config.php';
require_once '../server/TradeMessage.php';
require_once '../server/MessageProcessor.php';

class MessageProcessorTest extends PHPUnit_Framework_TestCase {
	
	
	public function testInstantiation() {
	
		$messageProcessor = new MessageProcessor();
	
		$this->assertInstanceOf( 'MessageProcessor', $messageProcessor );
	}
	
	
	public function testIsValid() {
		
		$tradeMessage = new TradeMessage( 1,
										'EUR',
										'USD',
										100,
										80,
										0.8,
										'4JAN15 10:27:44',
										'FR',
										2 );
		
		$messageProcessor = new MessageProcessor();
		$result = $messageProcessor->isValidTrade( $tradeMessage );
		$this->assertEquals( $result, TRUE );
	}
	
	
	public function testBadToCurrency() {
	
		$tradeMessage = new TradeMessage( 1,
										'EUR',
										'YU',
										100,
										80,
										0.8,
										'4JAN15 10:27:44',
										'FR',
										2 );
	
		$messageProcessor = new MessageProcessor();
		$result = $messageProcessor->isValidTrade( $tradeMessage );
		$this->assertEquals( $result, FALSE );
	}
	
	
	public function testBadFromCurrency() {
	
		$tradeMessage = new TradeMessage( 1,
										'AUS',
										'USD',
										100,
										80,
										0.8,
										'4JAN15 10:27:44',
										'FR',
										2 );
	
		$messageProcessor = new MessageProcessor();
		$result = $messageProcessor->isValidTrade( $tradeMessage );
		$this->assertEquals( $result, FALSE );
	}
	
	
	public function testZeroSellAmount() {
	
		$tradeMessage = new TradeMessage( 1,
										'EUR',
										'USD',
										0,
										80,
										0.8,
										'4JAN15 10:27:44',
										'FR',
										2 );
	
		$messageProcessor = new MessageProcessor();
		$result = $messageProcessor->isValidTrade( $tradeMessage );
		$this->assertEquals( $result, FALSE );
	}
	
	
	public function testZeroBuyAmount() {
	
		$tradeMessage = new TradeMessage( 1,
										'EUR',
										'USD',
										100,
										0,
										0.8,
										'4JAN15 10:27:44',
										'FR',
										2 );
	
		$messageProcessor = new MessageProcessor();
		$result = $messageProcessor->isValidTrade( $tradeMessage );
		$this->assertEquals( $result, FALSE );
	}
	
	
	public function testNegativeSellAmount() {
	
		$tradeMessage = new TradeMessage( 1,
										'EUR',
										'USD',
										-100,
										80,
										0.8,
										'4JAN15 10:27:44',
										'FR',
										2 );
	
		$messageProcessor = new MessageProcessor();
		$result = $messageProcessor->isValidTrade( $tradeMessage );
		$this->assertEquals( $result, FALSE );
	}
	
	
	public function testNegativeBuyAmount() {
	
		$tradeMessage = new TradeMessage( 1,
										'EUR',
										'USD',
										100,
										-80,
										0.8,
										'4JAN15 10:27:44',
										'FR',
										2 );
	
		$messageProcessor = new MessageProcessor();
		$result = $messageProcessor->isValidTrade( $tradeMessage );
		$this->assertEquals( $result, FALSE );
	}	
	
	
	public function testZeroRate() {
	
		$tradeMessage = new TradeMessage( 1,
										'EUR',
										'USD',
										100,
										80,
										0,
										'4JAN15 10:27:44',
										'FR',
										2 );
	
		$messageProcessor = new MessageProcessor();
		$result = $messageProcessor->isValidTrade( $tradeMessage );
		$this->assertEquals( $result, FALSE );
	}
	
}


?>