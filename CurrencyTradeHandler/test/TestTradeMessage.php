<?php

require_once '../server/TradeMessage.php';

class TradeMessageTest extends PHPUnit_Framework_TestCase {
	

	public function testInstantiation() {
	
		$tradeMessage = new TradeMessage( 1,
										'EUR',
										'USD',
										100,
										80,
										0.8,
										'4JAN15 10:27:44',
										'FR',
										2 );
		
		$this->assertInstanceOf( 'TradeMessage', $tradeMessage );
		
	}
	
	/**
	 * @expectedException Exception
	 */
	public function testRateException() {
		
		
		$tradeMessageNonNumericRate = new TradeMessage( 1,
														'EU',
														'USD',
														100,
														80,
														'rate',
														'4JAN15 10:27:44',
														'FR',
														2 );
	}
		
	
	/**
	 * @expectedException Exception
	 */
	public function testAmountBuyException() {
		
		$tradeMessageNonNumericAmountBuy = new TradeMessage( 1,
															'EUR',
															'USD',
															'one hundred',
															80,
															0.8,
															'4JAN15 10:27:44',
															'FR',
															2 );
	}
	
	/**
	 * @expectedException Exception
	 */
	public function testAmountSellException() {
		
		$tradeMessageNonNumericAmountSell = new TradeMessage( 1,
															'EUR',
															'USD',
															100,
															'eighty',
															0.8,
															'4JAN15 10:27:44',
															'FR',
															2 );
	}
	
	
	
	public function testtoJSON() {
		
		$tradeMessage = new TradeMessage( 1,
										'EUR',
										'USD',
										100,
										80,
										0.8,
										'24JAN15 10:27:44',
										'FR',
										2 );
		
 		$json = $tradeMessage->toJSON();
		
 		$expectedJson = '{"userId":1,"currencyFrom":"EUR","currencyTo":"USD","amountSell":100,"amountBuy":80,"rate":0.8,"timePlaced":"24JAN15 10:27:44","originatingCountry":"FR","status":2}';

		$this->assertEquals( $json, $expectedJson );
	}
}


?>