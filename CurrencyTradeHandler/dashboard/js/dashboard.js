
var urlBase = 'http://localhost/git/CurrencyTradeHandler/CurrencyTradeHandler/';

// Having token hard-coded like this into the JavaScript is of course totally insecure!!
// But as this is an assessment I didn't want to oblige the user to login the UI
// In "reality" this would be retrieved from the server session so only logged in users could access the API
var apiToken = 'w3aswudR';

$(document).ready(function(){		
	
	$( '#clear_notif_btn' ).bind( "click", function() {
		clearNotifications();	
	});

	$( '#clear_all_btn' ).bind( "click", function() {
		clearTransactions();
		clearNotifications();
	});
	
	updateAll();
	
	var refreshPage = function () {
		  updateAll();
		};
		setInterval( refreshPage, 1000 );
}); 


function clearNotifications() {

	$.post( urlBase + 'api/clearNotifications', function( data ) {
		$( ".result" ).html( data );
		updateNotifications();
	});	
}


function clearTransactions() {

	$.post( urlBase + 'api/resetAllTrades', function( data ) {
		$( ".result" ).html( data );
		updateTransactions();
	});
	
	$.post( urlBase + 'api/resetAllTransactionCounters', function( data ) {
		$( ".result" ).html( data );
		updateTransactionCounters();
	});
	
	$.post( urlBase + 'api/resetAllCurrencyBalances', function( data ) {
		$( ".result" ).html( data );
		updateCurrencyBalances();
	});
}


function updateAll() {

	updateNotifications();	
	updateCurrencyBalances();	
	updateTransactionCounters();	
	updateTransactions();	
}


function updateNotifications() {	
	
	$.get( urlBase + 'api/allNotifications/' + apiToken, function( response ) {
		
		var notifs, numNotifs, i, rowHTML, tableHTML = [];
		notifs = $.parseJSON( response );
		numNotifs = notifs.length;
		
		tableHTML.push( '<table><tr><td>Type</td><td>Details</td><td>Message</td></tr>' );
			
		// Display most recent transactions first!
		for ( i = (numNotifs - 1); i >= 0; i-- ) {	
			if ( notifs[i].trade !== null ) {
				rowHTML = '<tr><td>' + notifs[i].type + '</td><td>' + notifs[i].details + '</td><td></td></tr>';
			} else {
				rowHTML = '<tr><td>' + notifs[i].type + '</td><td>' + notifs[i].details + '</td><td></td></tr>';
			}
			tableHTML.push( rowHTML );		
		}
		tableHTML.push( '</table>' );
		$( "#notification_record_div" ).html( tableHTML.join('') );
	});
}


function updateTransactions() {
	
	$.get( urlBase + 'api/allTransactions/' + apiToken, function( response ) {
	
		var trans, numTrans, i, rowHTML, rowClass, friendlyStatus, tableHTML = [];
		trans = $.parseJSON( response );
		numTrans = trans.length;
		
		tableHTML.push( '<table><tr><td>User ID</td><td>Currency From</td><td>Currency To</td><td>Amount Sell</td><td>Amount Buy</td><td>Rate</td><td>Time Placed</td><td>Originating Country</td><td>Status</td></tr>' );
		// Display most recent transactions first!
		for ( i = (numTrans - 1); i >= 0; i-- ) {

			rowClass = (trans[i].status == 2 ? 'normal' : 'alertRow' );
			
			if ( trans[i].status == 0 ) {
				friendlyStatus = 'Not Processed';
			} else if ( trans[i].status == -1 ) {
				friendlyStatus = 'Invalid';
			} else if ( trans[i].status == 1 ) {
				friendlyStatus = 'Valid';
			} else if ( trans[i].status == 2 ) {
				friendlyStatus = 'Processed';
			}
			
			rowHTML = '<tr class="' + rowClass + '"><td>' + trans[i].userId + '</td><td>' + trans[i].currencyFrom + '</td><td>' +
								   							trans[i].currencyTo + '</td><td>' + trans[i].amountSell + '</td><td>' + 
								   							trans[i].amountBuy + '</td><td>' + trans[i].rate + '</td><td>' + 
								   							trans[i].timePlaced + '</td><td>' + trans[i].originatingCountry + '</td><td>' + 
								   							friendlyStatus + '</td><td></td></tr>';
			
			tableHTML.push( rowHTML );		
		}
		tableHTML.push( '</table>' );
		$( "#trans_record_div" ).html( tableHTML.join('') );
	});
}


function updateCurrencyBalances() {

	$.get( urlBase + 'api/allCurrencyBalances/' + apiToken, function( response ) {
		
		var balanceArray, balanceList;
		var balanceEURText, balanceUSDText, balanceBRLText, balanceGBPText, balanceSEKText;		
		balances = $.parseJSON( response );
		// TODO Should really populate the range of possible currencies dynamically when rendering the page - i.e. in PHP
		balanceArray = [parseInt(balances.EUR), parseInt(balances.USD), parseInt(balances.BRL), parseInt(balances.GBP), parseInt(balances.SEK) ];
		
		$.jqplot( 'balance_plot_div', [ balanceArray ], { title: 'Currency Balances',
											      		  seriesDefaults:{ renderer:$.jqplot.BarRenderer, rendererOptions: {fillToZero: true} },
											      		  series:[{renderer:$.jqplot.BarRenderer}],														  
											      		  seriesColors: [ "#4bb2c5", "#c5b47f", "#EAA228", "#579575", "#839557", "#958c12"],
											      		  } );

		balanceEURText = '<span style="color:#4bb2c5;"><b>EUR: </b>' + parseInt(balances.EUR) + '</span><br />';
		balanceUSDText = '<span style="color:#c5b47f;"><b>USD: </b>' + parseInt(balances.USD) + '</span><br />';
		balanceBRLText = '<span style="color:#EAA228;"><b>BRL: </b>' + parseInt(balances.BRL) + '</span><br />';
		balanceGBPText = '<span style="color:#579575;"><b>GBP: </b>' + parseInt(balances.GBP) + '</span><br />';
		balanceSEKText = '<span style="color:#839557;"><b>SEK: </b>' + parseInt(balances.SEK) + '</span><br />';
		
		balanceList = balanceEURText + balanceUSDText + balanceBRLText + balanceGBPText + balanceSEKText;	
		$( "#balance_list_div" ).html( balanceList );
		
	});	
}


function updateTransactionCounters() {
	
	$.get( urlBase + 'api/allTransactionCounters/' + apiToken, function( response ) {
			
		var transactionArray, transactionList;
		var transactionFRText, transactionUSText, transactionBRText, transactionGBText, transactionSEText;		
		transactions = $.parseJSON( response );
		
		// TODO Should really populate the range of possible countries dynamically when rendering the page - i.e. in PHP
		transactionArray = [parseInt(transactions.FR), parseInt(transactions.US), parseInt(transactions.BR), parseInt(transactions.GB), parseInt(transactions.SE) ];
		
		$.jqplot( 'trans_counter_chart_div', [ transactionArray ], { title: 'Transaction Counters',
											      		  seriesDefaults:{ renderer:$.jqplot.BarRenderer, rendererOptions: {fillToZero: true} },
											      		  series:[{renderer:$.jqplot.BarRenderer}],														  
											      		  seriesColors: [ "#4bb2c5", "#c5b47f", "#EAA228", "#579575", "#839557", "#958c12"],
											      		  } );

		transactionFRText = '<span style="color:#4bb2c5;"><b>France: </b>' + parseInt(transactions.FR) + '</span><br />';
		transactionUSText = '<span style="color:#c5b47f;"><b>United States: </b>' + parseInt(transactions.US) + '</span><br />';
		transactionBRText = '<span style="color:#EAA228;"><b>Brazil: </b>' + parseInt(transactions.BR) + '</span><br />';
		transactionGBText = '<span style="color:#579575;"><b>Great Britain: </b>' + parseInt(transactions.GB) + '</span><br />';
		transactionSEText = '<span style="color:#839557;"><b>Sweden: </b>' + parseInt(transactions.SE) + '</span><br />';
		
		transactionList = transactionFRText + transactionUSText + transactionBRText + transactionGBText + transactionSEText;	
		$( "#trans_counter_list_div" ).html( transactionList );		
		
	});
}