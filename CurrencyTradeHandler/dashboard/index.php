<!doctype html>
<html>
	<head>
	<meta charset="UTF-8">
	<title>Currency Trade Handler Dashboard</title>

		
	<link type="text/css" rel="stylesheet" media="screen" href="css/compressed/baseline.compress.css">
	<link type="text/css" rel="stylesheet" media="screen" href="css/jquery.jqplot.min.css">
	<link type="text/css" rel="stylesheet" media="screen" href="css/dashboard.css">

 	<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
 	<script type="text/javascript" src="js/jquery.jqplot.min.js"></script>
 	<script type="text/javascript" src="js/jqplot.barRenderer.min.js"></script>	
 	
	<script type="text/javascript" src="js/dashboard.js"></script>
		
	</head>
	
	<body class="page-typography">
		<div id="page">
			
			<header id="branding">
			<h1>Currency Trade Handler Dashboard</h1>			
			</header>
			
			<p>&copy; Michael Cullen, 2015</p>
			<br />
			<div id="notifications_div">
				<h2>Notifications</h2>
				<div id="notification_record_div" class="details_div">
				</div>
				<br />
				<input id="clear_notif_btn" type="submit" value="Clear Notifications" class="flat-button">
			</div>
			
			<h2>Plots</h2>
			<div id="graphical_data_div">
						
				<div id="lhs_graph_div">
					
					<div id="balance_plot_div"></div>
					<div id="balance_list_div"></div>
				</div>
				<div id="rhs_graph_div">
					<div id="trans_counter_chart_div"></div>
					<div id="trans_counter_list_div"></div>
				</div>
				
			</div>
			<div style="clear: both;"></div>
	
			<div id="record_div">
				<h2>Transaction Record</h2>
				<div id="trans_record_div" class="details_div">
				</div>
				Transactions shown in red are invalid and have not updated the currency balances or been added to the transaction counters.
				<br />
				<input id="clear_all_btn" type="submit" value="Clear All Transactions" class="flat-button">
			</div>
		</div>

	</body>
</html>