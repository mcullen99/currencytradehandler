General Approach
----------------
I have concentrated on code clarity and on building an architecture that can be easily extended going forwards.
The application as developed uses the file system as its storage layer. Clearly this is a significant limitation for performance and, since PHP is not thread-safe, offers the risk of multiple processes attempting to access a shared resource at the same time. For this reason all i/o is handled through a single datastore class. It would be easy to replace this file-based datastore with a relational or other database system if required.

Security
--------
I have noted that the assessment will take the security of the application into account, but I am conscious that the application needs to be accessible for evaluation (for which I cannot provide user credentials) and that automated tools may be used to POST calls to the API. Accordingly I have not  protected the dashboard with a login. Similarly I have not placed any requirements on the form of the userId received in the POST call, as I did know what the range of valid userIds might be. 
GET calls do not return data unless a user token is sent along with the call. For the purposes of this demo only, the token is hardcoded in the frontend JavaScript. I am well aware that this is not good practice!!

Libraries and Frameworks
------------------------
I have concieved this as a relatively lightweight application and have not used a "full-fat" framework such as Symfony or the Zend Framework as I would not be using the vast majority of their facilities. However I have used the Slim Framework to implement the RESTful API. 
I have used jQuery and the jsPlot plugin for the client dashboard component, and the baseline.css library as a starting point for the dashboard styling.