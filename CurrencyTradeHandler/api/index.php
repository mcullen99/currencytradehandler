<?php


require_once '../lib/Slim/Slim/Slim.php';
require_once '../server/TradeMessage.php';
require_once '../server/TransactionCounter.php';
require_once '../server/Balance.php';
require_once '../server/Notification.php';
require_once '../server/MessageProcessor.php';
require_once '../server/DataStore.php';
require_once '../server/Config.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();



$app->get('/allNotifications/:token', function ( $token ) {
	
	$dataStore = new DataStore();
	$notifications = $dataStore->getAllNotifications();
	echo ( $token === Config::$apiToken ? $notifications : "" );
	exit;
});


$app->get('/allTransactions/:token', function ( $token ) {
	
	$dataStore = new DataStore();
	$transactions = $dataStore->getAllTrades();
	echo ( $token === Config::$apiToken ? $transactions : "" );	
	exit;
});


$app->get('/lastTransaction/:token', function ( $token ) {

	$dataStore = new DataStore();
	$transaction = $dataStore->getLastTrade();
	echo ( $token === Config::$apiToken ? $transaction : "" );
	exit;
});
	

$app->get('/allCurrencyBalances/:token', function ( $token ) {

	$dataStore = new DataStore();
	$currencyBalances = $dataStore->getAllCurrencyBalances();
	echo ( $token === Config::$apiToken ? json_encode( $currencyBalances ) : "" );
	exit;
});



$app->get('/allTransactionCounters/:token', function ( $token ) {

	$dataStore = new DataStore();
	$transactionCounters = $dataStore->getAllCountryTransCounters();
	echo ( $token === Config::$apiToken ? json_encode( $transactionCounters ) : "" );
	exit;
});

	

$app->post( '/processTrade/:message', function ($msgJSON) {
	
	
	try {
		
		$tradeMessage = buildMessage ( $msgJSON );
						
		$processor = new MessageProcessor();		
		if ( !$processor->isValidTrade($tradeMessage) ) {
		
			$tradeMessage->setStatus( TradeMessage::INVALID );
			$tradeMessage->store();
			
			error_log( 'The Trade Message was not valid: ' . $msgJSON );
			$notification = new Notification( Notification::INVALID_MESSAGE, 'The Trade Message was not valid', $msgJSON );
			$notification->store();
			
		} else if ( !$processor->isWithinUserLimits($tradeMessage) ) {
		
			$tradeMessage->setStatus( TradeMessage::NOT_PROCESSED );
			$tradeMessage->store();
			
			$errorText = 'User ' . $tradeMessage.getUserId() . ' has temporarily exceeded rate limits at ' . $tradeMessage.getTimePlaced();
			error_log ( $errorText );
			$notification = new Notification( Notification::RATE_LIMIT_EXCEEDED, $errorText, $tradeMessage.toJSON() );
			$notification->store();
		
		} else {
			
			$tradeMessage->setStatus( TradeMessage::PROCESSSED );
			$tradeMessage->store();			
			echo 'Trade Message Stored';
		}
		
		
		
	} catch ( Exception $ex ) {
		
		error_log( $ex->getMessage() );
		
		if ( $ex->getCode() == 0 ) {
			$notification = new Notification( Notification::INVALID_JSON, $ex->getMessage() );
			echo 'Trade Message contains invalid JSON';
		} else if ( $ex->getCode() == 101 ) {
			$notification = new Notification( Notification::INVALID_JSON, $ex->getMessage() );
			echo 'Trade Message contains mismatched parameter type';
		} else if ( $ex->getCode() == 102 ) {
			$notification = new Notification( Notification::INVALID_JSON, $ex->getMessage() );
			echo 'Trade Message is missing a required parameter';
		}
		echo $ex->getMessage();
	}
	
	exit;
	
	
} );


$app->post( '/clearNotifications/', function () {

	$dataStore = new DataStore();
	$result = $dataStore->clearAllNotifications();
	echo $result;
	exit;
} );
	
	
$app->post( '/resetAllTrades/', function () {

	$dataStore = new DataStore();
	$result = $dataStore->resetAllTrades();
	echo $result;
	exit;
} );	


$app->post( '/resetAllTransactionCounters/', function () {

	$dataStore = new DataStore();
	$result = $dataStore->resetAllTransactionCounters();
	echo $result;
	exit;
} );


$app->post( '/resetAllCurrencyBalances/', function () {

	$dataStore = new DataStore();
	$result = $dataStore->resetAllCurrencyBalances();
	echo $result;
	exit;
} );
	
	
$app->run();



// Arguably this function should be in a dedicated MessageFactory class but this
// could be seen as OO overkill...
function buildMessage ( $msgJSON ) {

	$msg = json_decode( $msgJSON );
	
	if ( ($msg == FALSE) || ($msg == NULL) ) {
		throw new Exception( "Not Valid JSON", 0 );
	}

	try {
			
		$tradeMessage = new TradeMessage (  $msg->userId,
											$msg->currencyFrom,
											$msg->currencyTo,
											$msg->amountSell,
											$msg->amountBuy,
											$msg->rate,
											$msg->timePlaced,
											$msg->originatingCountry,
											TradeMessage::NOT_PROCESSED );

		return $tradeMessage;

	} catch ( Exception $ex ) {
		
		if ( $ex->getCode() == 8 ) {	
			throw new Exception( "Required Trade Message Parameter missing, \n" . $ex->getMessage(), 102 );
		} else if ( $ex->getCode() == 101 ) {
			throw new Exception( "Mismatched Parameter type in Trade Message, \n" . $ex->getMessage(), 101 );
			
		}
		
	}
}

?>