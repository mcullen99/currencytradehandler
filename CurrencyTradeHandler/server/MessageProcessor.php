<?php
class MessageProcessor {

	public function __construct() {

	}

	
	public function isValidTrade ( $tradeMessage ) {
		
		
		// If from currency is not valid
		
		if ( !in_array($tradeMessage->getCurrencyFrom(), Config::$validCurrencies) ) {
			error_log( 'Invalid Source Currency in message ' . $tradeMessage->toJSON() );
			return FALSE;
		}

		if ( !in_array($tradeMessage->getCurrencyTo(), Config::$validCurrencies) ) {
			error_log( 'Invalid Target Currency in message ' . $tradeMessage->toJSON() );
			return FALSE;
		}
		
		if ( !in_array($tradeMessage->getOriginatingCountry(), Config::$validCountries) ) {
			error_log( 'Invalid Originating Country in message ' . $tradeMessage->toJSON() );
			return FALSE;
		}
		
		if ( $tradeMessage->getAmountSell() == 0 ) {
			error_log( 'Invalid Transaction - zero sell amount in message ' . $tradeMessage->toJSON() );
			return FALSE;
		}

		if ( $tradeMessage->getAmountBuy() == 0 ) {
			error_log( 'Invalid Transaction - zero buy amount in message ' . $tradeMessage->toJSON() );
			return FALSE;
		}
		
		if ( $tradeMessage->getAmountSell() < 0 ) {
			error_log( 'Invalid Transaction - negative sell amount in message ' . $tradeMessage->toJSON() );
			return FALSE;
		}
		
		if ( $tradeMessage->getAmountBuy() < 0 ) {
			error_log( 'Invalid Transaction - negative buy amount in message ' . $tradeMessage->toJSON() );
			return FALSE;
		}
		
		if ( $tradeMessage->getRate() == 0 ) {
			error_log( 'Invalid Transaction - zero rate in message ' . $tradeMessage->toJSON() );
			return FALSE;
		}
		
		if ( $tradeMessage->getAmountSell() < 0 ) {
			error_log( 'Invalid Transaction - negative rate in message ' . $tradeMessage->toJSON() );
			return FALSE;
		}
		
		return TRUE;
	}

	
	// TODO Implement MessageProcessor->isWithinUserLimits
	public function isWithinUserLimits( $tradeMessage ) {
		return TRUE;
	}

}


?>