<?php


class Config {
	
	
	public static $validCurrencies = array ( 'EUR',
									  'USD',
									  'BRL',
									  'GBP', 
									  'SEK' );
	
	public static $validCountries = array ( 'FR',
									 'US',
									 'BR',
									 'GB',
									 'SE' );
	
	public static $rateLimit = 5;
	 
	public static $apiToken = 'w3aswudR';
}