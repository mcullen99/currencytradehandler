<?php

class TransactionCounter {
	
	private $country;
	
	public function __construct( $country ) {
		$this->country = $country;
	}
	
	
	public function increment() {
		
		$dataStore = new DataStore(); 
		$dataStore->incrementTransCounter( $this->country );
	}
	
	
	public function reset() {
		$dataStore = new DataStore();
		$dataStore->resetTransactionCounter( $this->country );
	}
}


?>