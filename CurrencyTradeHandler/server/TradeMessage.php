<?php

class TradeMessage {

	// Status constants
	const NOT_PROCESSED = 0;
	const INVALID = -1;
	const VALID = 1;
	const PROCESSSED = 2;
	
	
	private $userId;
	private	$currencyFrom;
	private $currencyTo;
	private $amountSell;
	private $amountBuy;
	private $rate;
	private $timePlaced;
	private $originatingCountry;
	private $status;


	public function __construct( $userId,
			$currencyFrom,
			$currencyTo,
			$amountSell,
			$amountBuy,
			$rate,
			$timePlaced,
			$originatingCountry,
			$status ) {

		$this->userId = $userId;
		$this->currencyFrom = $currencyFrom;
		$this->currencyTo = $currencyTo;
		if ( is_numeric($amountSell) ) {
			$this->amountSell = $amountSell;
		} else {
			throw new Exception( "Parameter 'amountSell' should be numeric", 101 );
		}
		if ( is_numeric($amountBuy) ) {
			$this->amountBuy = $amountBuy;
		} else {
			throw new Exception( "Parameter 'amountBuy' should be numeric", 101 );		}
		if ( is_numeric($rate) ) {
			$this->rate = $rate;
		} else {
			throw new Exception( "Parameter 'rate' should be numeric", 101 );
		}
		// TODO Should check Type of time placed also!!
		$this->timePlaced = $timePlaced;
		$this->originatingCountry = $originatingCountry;
		$this->status = $status;
	}


	public function getUserId() {
		return $this->userId;
	}

	public function setUserId( $userId ) {
		$this->userId = $userId;
	}


	public function getCurrencyFrom() {
		return $this->currencyFrom;
	}

	public function setCurrencyFrom( $currencyFrom ) {
		$this->currencyFrom = $currencyFrom;
	}

	public function getCurrencyTo() {
		return $this->currencyTo;
	}

	public function setCurrencyTo( $currencyTo ) {
		$this->currencyTo = $currencyTo;
	}

	public function getAmountSell() {
		return $this->amountSell;
	}

	public function setAmountSell( $amountSell ) {
		$this->amountSell = $amountSell;
	}

	public function getAmountBuy() {
		return $this->amountBuy;
	}

	public function setAmountBuy( $amountBuy ) {
		$this->amountBuy = $amountBuy;
	}

	public function getRate() {
		return $this->rate;
	}

	public function setRate( $rate ) {
		$this->rate = $rate;
	}

	public function getTimePlaced() {
		return $this->timePlaced;
	}

	public function setTimePlaced( $timePlaced ) {
		$this->timePlaced = $timePlaced;
	}

	public function getOriginatingCountry() {
		return $this->originatingCountry;
	}

	public function setOriginatingCountry( $originatingCountry ) {
		$this->originatingCountry = $originatingCountry;
	}
	
	public function getStatus() {
		return $this->status;
	}
	
	/**
	 * 
	 * @param string $status Should be one of the status constants defined at the top of the class
	 */
	public function setStatus( $status ) {
		$this->status = $status;
	}

	
	/**
	 * Store details of the transaction for audit purposes.
	 * Increment counters and update balances only if the transaction has been processed
	 * 
	 * If a customer is selling a currency, our balance of that currency is increased
	 * If a customer is buying a currency, our balance of that currency is decreased
	 */
	public function store() {
		
		$dataStore = new DataStore();
		$dataStore->recordTrade( $this );
		
		if ( $this->status === $this::PROCESSSED ) {
			$transactionCounter = new TransactionCounter( $this->getOriginatingCountry() );
			$transactionCounter->increment();
			
			$balanceFrom = new Balance( $this->getCurrencyFrom() );
			$balanceFrom->add( $this->getAmountSell() );
			
			$balanceTo = new Balance( $this->getCurrencyTo() );
			$balanceTo->subtract( $this->getAmountBuy() );
		}
	}
	
	
	/**
	 * Returns a JSON representation of TradeMessage
	 * (Necessary because the json_encode() function does not encode private members)
	 */
	public function toJSON() {
		
		return json_encode( array(
				'userId' => $this->getUserId(),
				'currencyFrom' => $this->getCurrencyFrom(),
				'currencyTo' => $this->getCurrencyTo(),
				'amountSell' => $this->getAmountSell(),
				'amountBuy' => $this->getAmountBuy(),
				'rate' => $this->getRate(),
				'timePlaced' => $this->getTimePlaced(),
				'originatingCountry' => $this->getOriginatingCountry(),
				'status' => $this->getStatus()			
		) );
	}


}


?>