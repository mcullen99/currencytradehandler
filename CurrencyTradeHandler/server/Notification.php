<?php

/**
 * Class for Notification messages that should be displayed to displayed to user.
 * 
 * @author Michael
 *
 */
class Notification {

	const INVALID_MESSAGE = 'INVALID MESSAGE';
	const RATE_LIMIT_EXCEEDED = 'RATE LIMIT EXCEEDED';
	const INVALID_JSON = 'INVALID JSON';

	private $type;
	private $details;
	private $trade;

	public function __construct( $type, $details, $trade ) {

		$this->type = $type;
		$this->details = $details;
		$this->trade = json_decode( $trade );
	}


	public function getDetails() {
		return $this->details;
	}

	
	public function getType() {
		return $this->type;
	}

	public function getTrade() {
		return $this->trade;
	}
	
	public function toJSON() {

		return json_encode( array(
				'type' => $this->getType(),
				'details' => $this->getDetails(),
				'trade' => $this->getTrade()
		) );
	}
	
	public function store() {
		
		$dataStore = new DataStore();
		$dataStore->addNotification( $this );
	}
}

?>