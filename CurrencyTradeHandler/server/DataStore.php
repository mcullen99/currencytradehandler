<?php

class DataStore {

	private $tradeRecordFile = '../data/tradeRecords.txt';
	private $notificationFile = '../data/Notifications.txt';
	
	public function recordTrade( $tradeMessage ) {
		
		file_put_contents( $this->tradeRecordFile, $tradeMessage->toJSON() . "\n", FILE_APPEND );
	}

	
	public function getAllTrades() {
	
		if ( file_exists($this->tradeRecordFile) ) {
			
			$trades = array();
			$tradeFile = fopen( $this->tradeRecordFile, 'r' );

			while ( !feof($tradeFile) ) {
				$trade = json_decode( fgets($tradeFile) );
				if ( $trade != null ) { 
					array_push( $trades, $trade );
				}
			}		
		} else {
			$trades = '';
		}
		return json_encode($trades);
	}
	
	
	public function getLastTrade() {
		
		$tradesJSON = $this->getAllTrades();		
		$trades = json_decode( $tradesJSON );
		if ( sizeof($trades) != 0 ) {
			return trim( json_encode($trades[1]) );
		} else {
			return '';
		}
	}
	
	
	public function incrementTransCounter( $country ) {
	
		$counterFile = $this->getCountryTransCounterFile( $country );
		
		if ( file_exists($counterFile) ) {
			$counter = file_get_contents( $counterFile );
			$counter++;
		} else {
			$counter = 1;
		}
		file_put_contents( $counterFile, $counter );
	}
	
	
	/**
	 * 
	 * @param string $currency
	 * @param float $amount The amount traded. Must be +ve for buy orders, and -ve for sell orders
	 */
	public function updateCurrencyBalance( $currency, $amount ) {
		
		$balanceFile = $this->getCurrencyBalanceFile( $currency );
		
		if ( file_exists($balanceFile) ) {
			$balance = file_get_contents( $balanceFile );
			$balance = $balance + $amount;
		} else {
			$balance = $amount;
		}
		file_put_contents( $balanceFile, $balance );
	}
	

	public function resetAllTrades() {
		
		if ( file_exists($this->tradeRecordFile) ) {
			unlink( $this->tradeRecordFile );
		}
	}
	
	
	public function resetTransactionCounter( $country ) {
		
		$counterFile = $this->getCountryTransCounterFile( $country );
		if ( file_exists($counterFile) ) {
			unlink( $counterFile );
		}
	}
	
	
	
	public function resetAllTransactionCounters() {
		
		$validCountries = Config::$validCountries;
		
		foreach ( $validCountries as $validCountry ) {
			$this->resetTransactionCounter( $validCountry );
		}
	}
	
	
	
	public function resetCurrencyBalance( $currency ) {
	
		$currencyFile = $this->getCurrencyBalanceFile( $currency );
		if ( file_exists($currencyFile) ) {
			unlink( $currencyFile );
		}
	}
	
	
	
	public function resetAllCurrencyBalances() {
	
		$validCurrencies = Config::$validCurrencies;
	
		foreach ( $validCurrencies as $validCurrency ) {
			$this->resetCurrencyBalance( $validCurrency );
		}
	}
	
	
	public function addNotification( $notification ) {
				
		file_put_contents( $this->notificationFile, stripslashes($notification->toJSON()) . "\n", FILE_APPEND );
	}
	
	
	public function getAllNotifications() {
	
		if ( file_exists($this->notificationFile) ) {
				
			$notifications = array();
			$notificationFile = fopen( $this->notificationFile, 'r' );
		
			while ( !feof($notificationFile) ) {
				$notification = json_decode( fgets($notificationFile) );
				if ( $notification != null ) {
					array_push( $notifications, $notification );
				}
			}
		} else {
			$notifications = '';
		}
		return json_encode( $notifications );		
	}
	
	
	public function getCurrencyBalance( $currency ) {
		
		$balanceFile = $this->getCurrencyBalanceFile( $currency );
		
		if ( file_exists($balanceFile) ) {
			return file_get_contents( $balanceFile );
		} else {
			return 0;
		}
	}
	
	
	public function getAllCurrencyBalances() {
		
		$validCurrencies = Config::$validCurrencies;
		$balances = array();
		foreach ( $validCurrencies as $validCurrency ) {
			$balances[$validCurrency] = $this->getCurrencyBalance( $validCurrency );
		}
		
		return $balances;
	}
	
	
	public function getAllCountryTransCounters() {
		
		$validCountries = Config::$validCountries;
		$transactionCounters = array();
		foreach ( $validCountries as $validCountry ) {
			$transactionCounters[$validCountry] = $this->getCountryTransCounter( $validCountry );
		}
		
		return $transactionCounters;
	}
	
	
	public function getCountryTransCounter( $country ) {
		
		$transCountFile = $this->getCountryTransCounterFile( $country );
		
		if ( file_exists($transCountFile) ) {
			return file_get_contents( $transCountFile );
		} else {
			return 0;
		}
	}
	
	
	public function clearAllNotifications() {
		
		if ( file_exists($this->notificationFile) ) {
			unlink( $this->notificationFile );
		}
	}
	
	
	private function getCurrencyBalanceFile( $currencyCode ) {
	
		return '../data/balance' . $currencyCode . '.txt';
	}
	
	
	private function getCountryTransCounterFile( $countryCode ) {
	
		return '../data/transCount' . $countryCode . '.txt';
	}
	
}

?>