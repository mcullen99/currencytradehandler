<?php
/**
 * Represents the balance of each currency that we hold.
 * Ideally currency inflows would exactly match currency outflows!
 *
 */
class Balance {

	private $currency;

	public function __construct( $currency ) {
		$this->currency = $currency;
	}


	public function add( $amount ) {

		$dataStore = new DataStore();
		$dataStore->updateCurrencyBalance( $this->currency, $amount );
	}


	public function subtract( $amount ) {
	
		$dataStore = new DataStore();
		$dataStore->updateCurrencyBalance( $this->currency, ( -$amount) );
	}
	
	public function reset() {
		$dataStore = new DataStore();
		$dataStore->resetBalance( $this->currency );
	}
}


?>